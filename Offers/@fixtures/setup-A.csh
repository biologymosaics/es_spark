#!/bin/csh

## !!! Make sure to set the permissions to read only
chmod -R 555 .

# leaf
touch leaf.txt

# branch
mkdir branch

# jump
mkdir jumps

mkdir jumps/soft
touch jumps/soft/jump.from
ln -s jump.from jumps/soft/jump.to
echo "soft jump text" > jumps/soft/jump.from

mkdir jumps/hard
touch jumps/hard/jump.from
ln jumps/hard/jump.from jumps/hard/jump.to
echo "hard jump text" > jumps/hard/jump.from

# block device


#
# 4 -> "block device"
# 5 -> "character device"
# 6 -> "FIFO"
# 7 -> "socket"
