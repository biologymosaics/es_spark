#!/usr/bin/env python

# ./set-image.py image.png myfile

# better quality??
# ./set-image.py image.icns myfile

# Location of Generic Folder Icon
# /System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericFolderIcon.icns

import Cocoa
import sys

Cocoa.NSWorkspace.sharedWorkspace().setIcon_forFile_options_(
    Cocoa
    .NSImage
    .alloc()
    .initWithContentsOfFile_(
        sys.argv[1]
        .decode('utf-8')
    ),
    sys
    .argv[2]
    .decode('utf-8'),

    0
) or sys.exit("Unable to set file icon")
