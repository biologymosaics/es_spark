
// mocha '**/*.aux.up' --recursive --check-leaks --reporter spec -g "Branch Stream"

const streamBranch = require("./../../stream.up");
const strictEqualityCheck = require("XO/bracket/strict-equality-check.aux.xo");

describe("Branch Stream", () => {
	describe("success checks", () => {
		it("[A] only leaves", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/A"
			);

			const places = [];

			streamBranch({
				branch,
				each: ({ good, model, lstats, next }) => {
					const place = require("path").relative(
						branch,
						good
					);

					places.push(place);
					next();
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "a.leaf" ]
					);

					done();
				}
			});
		});

		it("[B] leaves and branches, 1 story tall", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/B"
			);

			const places = [];
			const stats = [];

			streamBranch({
				branch,
				each: ({ good, model, lstats, next }) => {
					const place = require("path").relative(
						branch,
						good
					);
					stats.push(lstats);
					places.push(place);

					next();
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "A" ],
						[ places[1], "A/another.leaf" ],
						[ places[2], "a.leaf" ],
						[ stats[1].size, 22],
						[ stats[2].size, 9 ],
						[ stats[1].hasOwnProperty("ino"), true ],
						[ stats[2].hasOwnProperty("ino"), true ],
					);

					done();
				}
			});
		});

		it("[C] leaves and branches, 2 story tall", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/C"
			);

			const places = [];

			streamBranch({
				branch,
				each: ({ good, model, next }) => {
					const place = require("path").relative(
						branch,
						good
					);

					places.push(place);
					next();
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "A" ],
						[ places[1], "A/X" ],
						[ places[2], "A/X/asdfjio.erwej" ],
						[ places[3], "A/another.leaf" ],
						[ places[4], "a.leaf" ]
					);

					done();
				}
			});
		});

		it("[D] leaves, jumps, and branches. Branches are 2 stories tall.", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/D"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				each: ({ good, model, next }) => {
					const place = require("path").relative(
						branch,
						good
					);

					places.push(place);
					models.push(model);

					next();
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "A" ],
						[ places[1], "A/X" ],
						[ places[2], "A/X/asdfjio.erwej" ],
						[ places[3], "A/another.leaf" ],
						[ places[4], "a.leaf" ],
						[ places[5], "b.leaf" ],

						[ models[0], 1 ],
						[ models[1], 1 ],
						[ models[2], 2 ],
						[ models[3], 2 ],
						[ models[4], 2 ],
						[ models[5], 3 ]
					);

					done();
				}
			});
		});

		it("[D] leaves, jumps, and branches. Branches are 2 stories tall. Relative Locations", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/D"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				relativeLocations: true,
				each: ({ good, model, next }) => {
					places.push(good);
					models.push(model);

					next();
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "A" ],
						[ places[1], "A/X" ],
						[ places[2], "A/X/asdfjio.erwej" ],
						[ places[3], "A/another.leaf" ],
						[ places[4], "a.leaf" ],
						[ places[5], "b.leaf" ],

						[ models[0], 1 ],
						[ models[1], 1 ],
						[ models[2], 2 ],
						[ models[3], 2 ],
						[ models[4], 2 ],
						[ models[5], 3 ]
					);

					done();
				}
			});
		});

		it("[D] leaves, jumps, and branches. Branches are 2 stories tall. Relative Locations", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/D"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				relativeLocations: true,
				each: ({ good, model, next }) => {
					places.push(good);
					models.push(model);

					next();
				},
				sort: (A, B) => {
					const { model: modelA } = A;
					const { model: modelB } = B;

					return modelA > modelB;
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "b.leaf" ],
						[ places[1], "a.leaf" ],
						[ places[2], "A" ],
						[ places[3], "A/another.leaf" ],
						[ places[4], "A/X" ],
						[ places[5], "A/X/asdfjio.erwej" ],

						[ models[0], 3 ],
						[ models[1], 2 ],
						[ models[2], 1 ],
						[ models[3], 2 ],
						[ models[4], 1 ],
						[ models[5], 2 ]
					);

					done();
				}
			});
		});

		it("[E] leaves, jumps, and branches. Branches are 2 stories tall. Relative Locations. Sorted.", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/E"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				relativeLocations: true,
				each: ({ good, model, next }) => {
					places.push(good);
					models.push(model);

					next();
				},
				sort: (A, B) => {
					const { model: modelA } = A;
					const { model: modelB } = B;

					return modelA > modelB;
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "b.jump" ],
						[ places[1], "a.leaf" ],
						[ places[2], "Z" ],
						[ places[3], "Z/zz.leaf" ],
						[ places[4], "A" ],
						[ places[5], "A/another.leaf" ],
						[ places[6], "A/X" ],
						[ places[7], "A/X/asdfjio.erwej" ],

						[ models[0], 3 ],
						[ models[1], 2 ],
						[ models[2], 1 ],
						[ models[3], 2 ],
						[ models[4], 1 ],
						[ models[5], 2 ],
						[ models[6], 1 ],
						[ models[7], 2 ]
					);

					done();
				}
			});
		});

		it("[E] leaves, jumps, and branches. Branches are 2 stories tall. Relative Locations. Sorted.", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/E"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				relativeLocations: true,
				each: ({ good, model, next }) => {
					places.push(good);
					models.push(model);

					next();
				},
				sort: (A, B) => {
					const { model: modelA } = A;
					const { model: modelB } = B;

					return modelA < modelB;
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "Z" ],
						[ places[1], "Z/zz.leaf" ],
						[ places[2], "A" ],
						[ places[3], "A/X" ],
						[ places[4], "A/X/asdfjio.erwej" ],
						[ places[5], "A/another.leaf" ],
						[ places[6], "a.leaf" ],
						[ places[7], "b.jump" ],

						[ models[0], 1 ],
						[ models[1], 2 ],
						[ models[2], 1 ],
						[ models[3], 1 ],
						[ models[4], 2 ],
						[ models[5], 2 ],
						[ models[6], 2 ],
						[ models[7], 3 ]
					);

					done();
				}
			});
		});

		it("[E] without climbing branches", (done) => {
			const branch = require("path").resolve(
				__dirname,
				"@fixtures/stream/E"
			);

			const places = [];
			const models = [];

			streamBranch({
				branch,
				relativeLocations: true,
				climbBranches: false,
				each: ({ good, model, next }) => {
					places.push(good);
					models.push(model);

					next();
				},
				sort: (A, B) => {
					const { model: modelA } = A;
					const { model: modelB } = B;

					return modelA < modelB;
				},
				success: () => {
					strictEqualityCheck(
						[ places[0], "Z" ],
						[ places[1], "A" ],
						[ places[2], "a.leaf" ],
						[ places[3], "b.jump" ],

						[ models[0], 1 ],
						[ models[1], 1 ],
						[ models[2], 2 ],
						[ models[3], 3 ]
					);

					done();
				}
			});
		});
	});
});
