

/*
	NEEDS BRANCH EVO or BRANCH EQUALITY CHECK for testing purposes
*/


// mocha "**/*.broma.up" --recursive -g "Branch Mimic"
// mocha "**/*.broma.up" --recursive -g "Two Story Branch with leaves"


/*
	Depends on:
		produceBranchEvo
		eraseBranch
*/

const strictEqualityCheck = require("XO/bracket/strict-equality-check.aux.xo");

const fixtures = require("path").resolve(__dirname, "@fixtures");
const temp = require("path").resolve(__dirname, "@temp");

const mimicBranch = require("./../../start.up");
const produceBranchEvo = require("./../../../evo/produce.up")
const eraseBranch = require("./../../../erase/start.up")

const tests = [{
	name: "Branch with one leaf",
	branch: "A"
},{
	name: "Two Story Branch with leaves",
	branch: "B"
},{
	name: "Three Story Branch with leaves",
	branch: "C"
},{
	name: "Branch with a jump to a leaf",
	branch: "F"
}]

describe("Branch Mimic", () => {
	describe("success checks", () => {
		for (let i = 0; i <= tests.length - 1; i++) {
			const { name, branch } = tests[i];

			it(name, (done) => {
				const B = require("path").resolve(fixtures, branch);
				const E = require("path").resolve(temp, branch);

				mimicBranch({
					from: B,
					to: E,
					progress: (message, details) => {
						console.log(message, details);
					},
					success: () => {
						produceBranchEvo({
							branches: [ B, E ],
							filterEqual: true,
							success: ({ evo }) => {
								strictEqualityCheck({ evo },
									[ evo.length, 0 ]
								);

								eraseBranch({
									branch: E,
									success: () => {
										done();
									}
								});
							}
						});
					}
				});
			});
		}
	});

	describe("failure checks", () => {
		it(`fails with 14 on non-existent "from"`, (done) => {
			const branch = "D"; // non-existent

			const from = require("path").resolve(fixtures, branch);
			const to = require("path").resolve(temp, branch);

			mimicBranch({
				from,
				to,
				failure: (message, details) => {
					strictEqualityCheck({ message, details },
						[ message, 14 ]
					);

					done();
				},
				success: () => {
					throw new Error();
				}
			});
		});

		it(`fails with 13 when "from" is not a branch`, (done) => {
			const branch = "E.leaf";

			const from = require("path").resolve(fixtures, branch);
			const to = require("path").resolve(temp, branch);

			mimicBranch({
				from,
				to,
				failure: (message, details) => {
					strictEqualityCheck({ message, details },
						[ message, 13 ]
					);

					done();
				},
				success: () => {
					throw new Error();
				}
			});
		});
	});
});
