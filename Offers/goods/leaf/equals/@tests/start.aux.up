// mocha '**/*.aux.up' --recursive --check-leaks --reporter spec -g "Leaf Equals"


const Board = require("XO/Board/New.xo");

const eraseLeaf = require("./../../erase/start.up");
const leavesEqual = require("./../start.up");

const Processor = require("XO/Processor/New.xo");

const strictEqualityCheck = require("XO/bracket/strict-equality-check.aux.xo");

describe("Leaf Equals", () => {
	describe("equal", () => {
		describe("simple", () => {
			it("equal leafs", (done) => {
				const leafA = require("path").resolve(
					__dirname,
					"@fixtures/A/leaf.lefa",
				);

				const leafB = require("path").resolve(
					__dirname,
					"@fixtures/A/leafB.lefa"
				);

				leavesEqual({
					leaves: [
						leafA,
						leafB
					],
					yes: () => {
						done();
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			});

			it("equal leaf in leaf with other stuff", (done) => {
				const leafA = require("path").resolve(
					__dirname,
					"@fixtures/B/leaf.leaf",
				);

				const leafB = require("path").resolve(
					__dirname,
					"@fixtures/B/leaf-double.leaf"
				);

				leavesEqual({
					leaves: [{
						leaf: leafA,
						start: 0
					},{
						leaf: leafB,
						start: 4
					}],
					yes: () => {
						done();
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			});
		});

		describe("M video", () => {
			it("works", (done) => {
				// var readableA = require('fs').createReadStream(leaves[0], {
				// 	start: 0,
				// 	end: Infinity
				// });
				// var readableB = require('fs').createReadStream(leaves[1], {
				// 	start: 0,
				// 	end: Infinity
				// });

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;
				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				leavesEqual({
					leaves: [{
						leaf: leafA
					}, {
						leaf: leafB
					}],
					yes: () => {
						done();
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			});

			it("partial read", (done) => {
				const leafA = require('Melody/@fixtures/B/index.xo').mp4;
				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				leavesEqual({
					leaves: [{
						leaf: leafA,
						start: 10,
						end: 100
					}, {
						leaf: leafB,
						start: 10,
						end: 100
					}],
					yes: () => {
						done();
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			});
		});

		/*
			describe("high volume", () => {
			it("survives", (done) => {
				const processor = Processor({
					poll_interval: 1,
					capacity: 100
				});
			    processor.start();

				let counter = 0;
				const tryDone = () => {
					counter++;
					if (counter === 100) {
						processor.stop();

						// strictEqualityCheck(
						// 	[ order[0], 1 ],
						// 	[ order[1], 2 ],
						// 	[ order[2], 3 ]
						// );

						done();
					}
				}

				const leafA = require("path").resolve(
					__dirname,
					"@fixtures/A/leaf.lefa",
				);

				const leafB = require("path").resolve(
					__dirname,
					"@fixtures/A/leafB.lefa"
				);

				for (let i = 1; i <= 100; i++) {
					processor.add(({ complete }) => {
						leavesEqual({
	 	   				leaves: [
	 	   					leafA,
	 	   					leafB
	 	   				],
	 	   				yes: () => {
							complete();
	 	   					tryDone();
	 	   				},
	 	   				no: () => {
	 	   					throw new Error(`Expected yes, got no`);
	 	   				}
	 	   			});
				   });
				}
			});

			it("survives mp4", (done) => {
				const processor = Processor({
					poll_interval: 1,
					capacity: 100
				});
			    processor.start();

				let counter = 0;
				const tryDone = () => {
					counter++;
					if (counter === 1) {
						processor.stop();

						// strictEqualityCheck(
						// 	[ order[0], 1 ],
						// 	[ order[1], 2 ],
						// 	[ order[2], 3 ]
						// );

						done();
					}
				}

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;

				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				for (let i = 1; i <= 1; i++) {
					processor.add(({ complete }) => {
						leavesEqual({
		 	   				leaves: [
		 	   					leafA,
		 	   					leafB
		 	   				],
		 	   				yes: () => {
								complete();
		 	   					tryDone();
		 	   				},
		 	   				no: () => {
		 	   					throw new Error(`Expected yes, got no`);
		 	   				}
		 	   			});
				   });
				}
			});

			it("survives mp4 x2 at a time", function (done) {
				this.timeout(10000);

				const processor = Processor({
					poll_interval: 1,
					capacity: 100
				});
			    processor.start();

				let counter = 0;
				const tryDone = () => {
					counter++;
					if (counter === 1) {
						processor.stop();

						// strictEqualityCheck(
						// 	[ order[0], 1 ],
						// 	[ order[1], 2 ],
						// 	[ order[2], 3 ]
						// );

						done();
					}
				}

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;

				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				for (let i = 1; i <= 2; i++) {
					processor.add(({ complete }) => {
						leavesEqual({
	 	   				leaves: [
	 	   					leafA,
	 	   					leafB
	 	   				],
	 	   				yes: () => {
							complete();
	 	   					tryDone();
	 	   				},
	 	   				no: () => {
	 	   					throw new Error(`Expected yes, got no`);
	 	   				}
	 	   			});
				   });
				}
			});

			it("survives mp4 x4 at a time", function (done) {
				this.timeout(10000);

				const processor = Processor({
					poll_interval: 1,
					capacity: 100
				});
			    processor.start();

				let counter = 0;
				const tryDone = () => {
					counter++;
					if (counter === 1) {
						processor.stop();

						// strictEqualityCheck(
						// 	[ order[0], 1 ],
						// 	[ order[1], 2 ],
						// 	[ order[2], 3 ]
						// );

						done();
					}
				}

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;

				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				for (let i = 1; i <= 4; i++) {
					processor.add(({ complete }) => {
						leavesEqual({
	 	   				leaves: [
	 	   					leafA,
	 	   					leafB
	 	   				],
	 	   				yes: () => {
							complete();
	 	   					tryDone();
	 	   				},
	 	   				no: () => {
	 	   					throw new Error(`Expected yes, got no`);
	 	   				}
	 	   			});
				   });
				}
			});
		});
		*/
	});

	describe("non-equal", () => {
		describe("simple", () => {
			it("two non equal leaves, full read", (done) => {
				const leafA = require("path").resolve(
					__dirname,
					"@fixtures/A/leaf.lefa",
				);

				const leafC = require("path").resolve(
					__dirname,
					"@fixtures/A/not-equal.lefa"
				);

				leavesEqual({
					leaves: [
						leafA,
						leafC
					],
					yes: () => {
						throw new Error(`Expected no, got yes`);
					},
					no: () => {
						done();
					}
				});
			});
		});

		describe("M video", () => {
			it("works", (done) => {
				// var readableA = require('fs').createReadStream(leaves[0], {
				// 	start: 0,
				// 	end: Infinity
				// });
				// var readableB = require('fs').createReadStream(leaves[1], {
				// 	start: 0,
				// 	end: Infinity
				// });

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;
				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				leavesEqual({
					leaves: [{
						leaf: leafA,
						start: 1
					}, {
						leaf: leafB,
						start: 0
					}],
					yes: () => {
						throw new Error(`Expected no, got yes`);
					},
					no: () => {
						// strictEqualityCheck(
						// 	[ message, 2 ]
						// );

						done();
					}
				});
			});

			it("works", (done) => {
				// var readableA = require('fs').createReadStream(leaves[0], {
				// 	start: 0,
				// 	end: Infinity
				// });
				// var readableB = require('fs').createReadStream(leaves[1], {
				// 	start: 0,
				// 	end: Infinity
				// });

				const leafA = require('Melody/@fixtures/B/index.xo').mp4;
				const leafB = require('Melody/@fixtures/B/index.xo').mp4Mimic;

				leavesEqual({
					leaves: [{
						leaf: leafA,
						start: 0,
						end: 100000
					}, {
						leaf: leafB,
						start: 0,
						end: 100001
					}],
					yes: () => {
						throw new Error(`Expected no, got yes`);
					},
					no: () => {
						// strictEqualityCheck(
						// 	[ message, 2 ]
						// );

						done();
					}
				});
			});
		});
	});




});
