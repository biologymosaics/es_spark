
// mocha '**/*.aux.up' --recursive --check-leaks --reporter spec -g "Etch Stream"

/*
	The Testable
*/
const etchLeafStream = require("./../stream.up");

/*
	Depends on
*/
const readLeafStream = require("./../../read/stream.up");
const leavesEqual = require("./../../equals/start.up");
const eraseLeaf = require("./../../erase/start.up");

const video_short = require('Melody/@fixtures/C/index.xo');
const movie = require('Melody/@fixtures/B/index.xo');

describe("Etch Stream", () => {
	it("A, works", (done) => {
		const A = require("path").resolve(
			__dirname,
			"@fixtures/stream/A/leaf.A"
		);

		const A_to = require("path").resolve(
			__dirname,
			"@temp/stream/A/leaf.A"
		);

		etchLeafStream({
			leaf: A_to,
			ready: ({ etchStream }) => {
				readLeafStream({
					leaf: A,
					partial: ({
						strand,
						next,
						highWaterMark,
						position
					}) => {
						etchStream.etch(strand);
						next();
					},
					success: () => {
						etchStream.conclude();
					}
				});
			},
			success: () => {
				leavesEqual({
					leaves: [
						A,
						A_to
					],
					yes: () => {
						eraseLeaf({
							leaf: A_to,
							success: () => {
								done();
							}
						});
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			}
		});
	});

	it("B, works", (done) => {
		const fixture = require("path").resolve(
			__dirname,
			"@fixtures/stream/A/leaf.A"
		);

		const temp = require("path").resolve(
			__dirname,
			"@temp/stream/A/leaf.A"
		);

		etchLeafStream({
			leaf: temp,
			ready: ({ etchStream }) => {
				readLeafStream({
					leaf: fixture,
					partial: ({
						strand,
						next,
						highWaterMark,
						position
					}) => {
						etchStream.etch(strand);
						next();
					},
					success: () => {
						etchStream.stop();
						leavesEqual({
							leaves: [
								fixture,
								temp
							],
							yes: () => {
								eraseLeaf({
									leaf: temp,
									success: () => {
										setTimeout(() => {
											done();
										}, 400);
									}
								});
							},
							no: () => {
								throw new Error(`Expected yes, got no`);
							}
						});
					}
				});
			},
			success: () => {
				throw new Error();
			}
		});
	});

	it("16M Video", (done) => {
		const A = video_short.original;

		const A_to = require("path").resolve(
			__dirname,
			"@temp/stream/A/video-short.mp4"
		);

		etchLeafStream({
			leaf: A_to,
			ready: ({ etchStream }) => {
				readLeafStream({
					leaf: A,
					partial: ({
						strand,
						next,
						highWaterMark,
						position
					}) => {
						etchStream.etch(strand);
						next();
					},
					success: () => {
						etchStream.conclude();
					}
				});
			},
			success: () => {
				leavesEqual({
					leaves: [
						A,
						A_to
					],
					yes: () => {
						eraseLeaf({
							leaf: A_to,
							success: () => {
								done();
							}
						});
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			}
		});
	});

	it("449M Video", function(done) {
		this.timeout(5000);

		const A = movie.mp4;

		const A_to = require("path").resolve(
			__dirname,
			"@temp/stream/A/movie.mp4"
		);

		etchLeafStream({
			leaf: A_to,
			ready: ({ etchStream }) => {
				readLeafStream({
					leaf: A,
					partial: ({
						strand,
						next,
						highWaterMark,
						position
					}) => {
						etchStream.etch(strand);
						next();
					},
					success: () => {
						etchStream.conclude();
					}
				});
			},
			success: () => {
				leavesEqual({
					leaves: [
						A,
						A_to
					],
					yes: () => {
						eraseLeaf({
							leaf: A_to,
							success: () => {
								done();
							}
						});
					},
					no: () => {
						throw new Error(`Expected yes, got no`);
					}
				});
			}
		});
	});
});
