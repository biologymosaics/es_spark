#!/bin/csh

node-gyp configure;
node-gyp build;

mv build/Release/water.node water.bio.node

rm -rf build
