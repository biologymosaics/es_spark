
/*

	@goals
	const station = new Station({
		https: {
			port:
			cert:
			key:
		},
		http: {
			port:
		},

		each: ({ grant, wish, then }) => {
			const { url } = wish;

		},

		started: () => {

		},

		problems: (message, details) => {

		},

		// problems that can't be fixed
		failure: (message, details) => {

		},
	});

	station.start();

	// stop the station from accepting new connections
	// can be restarted via:
	//
	// station.start();
	//
	station.stop();


	// conclude the station
	station.conclude();
*/

const combineBrackets = require("XO/bracket/combine.xo");
const Board = require("XO/Board/New.xo");
const readListParallel = require("XO/list/parallel.xo")

const gatherLeafStream = require("./../goods/leaf/gather/stream.up");

function Holdings() {
	this.http = {
		server: null,
		ready: false,
		port: null
	}

	this.https = {
		server: null,
		ready: false,
		port: null,
	}

	this.on_failure = [];
}
Holdings.prototype = combineBrackets(Holdings.prototype, {
	init: function() {
		const holdings = this;

		function Macros(settings) {
			holdings.settings = settings;

			holdings.progress = settings.progress;

			holdings.failure = (m, d) => {
				// Maybe could be initiated in parallel
				for (let i = 0; i <= holdings.on_failure.length - 1; i++) {
					holdings.on_failure[i]();
				}

				holdings.settings.failure(m, d);
			};

			holdings.board = Board({
				failure: (message, details) => {
					settings.failure(message, details);
				}
			});
			holdings.board.start();

			holdings.on_failure.push(() => {
				holdings.board.stop();
			});

			holdings.board.add([{
				id: 1,
				fn: (task) => {
					holdings.setup_https({
						success: () => {
							holdings.progress(3);
							task.success();
						}
					});
				}
			},{
				id: 2,
				fn: (task) => {
					holdings.setup_http({
						success: () => {
							holdings.progress(4);
							task.success();
						}
					});
				}
			},{
				id: 3,
				and: [ 1, 2 ],
				fn: (task) => {
					task.success();
				}
			}]);

			return this;
		}
		Macros.prototype = combineBrackets(Macros.prototype, {
			start: function() {
				holdings.board.add([{
					id: 4,
					and: [ 3 ],
					fn: (task) => {
						holdings.start_http({
							success: () => {
								holdings.progress(5);
								task.success();
							}
						});
					}
				},{
					id: 5,
					and: [ 3 ],
					fn: (task) => {
						holdings.start_https({
							success: () => {
								holdings.progress(6);
								task.success();
							}
						});
					}
				},{
					id: 6,
					and: [ 4, 5 ],
					fn: (task) => {
						holdings.settings.started();
						task.success();
					}
				}]);
			},
			conclude: function() {
				holdings.board.add([{
					id: 7,
					and: [ 6 ],
					fn: (task) => {
						holdings.http.server.close(() => {
							task.success();
						});
					}
				},{
					id: 8,
					and: [ 7 ],
					fn: (task) => {
						holdings.https.server.close(() => {
							task.success();
						});
					}
				},{
					id: 9,
					and: [ 8, 7 ],
					fn: (task) => {
						holdings.board.stop();
						holdings.settings.concluded();
					}
				}]);

			}
		});

		return Macros;
	},

	setup_http: function({ success }) {
		const holdings = this;
		this.http.server = require('http').createServer(function (req, res) {
			holdings.redirect_http_to_https({
				req,
				res
			});
		});

		success();
	},
	start_http: function({ success }) {
		const holdings = this;
		const { port } = holdings.settings.http;

		this.http.server.listen(port, function() {
			success();
		});
	},

	setup_https: function({ success }) {
		const holdings = this;
		const { key, cert } = holdings.settings.https;
		const { settings } = this;

		const list = [];

		// let options = {
		// 	key: require("fs").readFileSync(key),
		// 	cert: require("fs").readFileSync(cert)
		// }

		let options = {};
		if (
			typeof key === "string" &&
			typeof cert === "string"
		) {
			list.push((complete) => {
				options.key = "";

				gatherLeafStream({
					leaf: key,
					strandSize: 1000,
					from: 1,
					to: "end",

					encoding: "utf8",
					each: ({
						glyph,
						next,
						conclude
					}) => {
						options.key += glyph;
						next();
					},
					success: () => {
						holdings.progress(1);
						complete();
					},
					failure: (message, details) => {

						holdings.failure(1, {
							message,
							details
						});
					}
				});
			});

			list.push((complete) => {
				options.cert = "";

				gatherLeafStream({
					leaf: cert,
					strandSize: 1000,
					from: 1,
					to: "end",

					encoding: "utf8",
					each: ({
						glyph,
						next,
						conclude
					}) => {
						options.cert += glyph;
						next();
					},
					success: () => {
						holdings.progress(2, {});
						complete();
					},
					failure: (message, details) => {
						holdings.failure(2, {
							message,
							details
						});
					}
				});
			});
		}


		readListParallel({
			list,
			each: function(entry, index, complete) {
				entry(complete);
			},

			// called automatically if !(list.length >= 1)
			success: function() {
				holdings.https.server = require('https').createServer(options, function(req, res) {
					const { url } = req;
					const wish = {
						req,
						url
					};
					const grant = {
						res
					};
					const custom = {};

					settings.before({
						wish,
						grant,
						custom,
						then: () => {
							settings.each({
								wish,
								grant,
								custom,
								then: () => {
									settings.after({
										wish,
										grant,
										custom
									});
								}
							});
						}
					});
				});

				success();
			}
		});
	},
	start_https: function({ success }) {
		const holdings = this;
		const { port } = holdings.settings.https;

		this.https.server.listen(port, function() {
			success();
		});
	},
	redirect_http_to_https: function({ req, res }) {
		const {
			port: https_port
		} = this.settings.https;

		const {
			url,
			method,
			headers,
			httpVersion
		} = req;
		const { host } = headers;

		// console.log("http request:", {
		// 	host: req.headers.host,
		// 	url: req.url
		// });

		const full_url = "https://" + host + url;
		const parsed_url = require("url").parse(full_url);

		const redirect_location = "https://" + parsed_url.hostname + ":" + https_port + url;

		// const location = "https://" + req.headers['host'] + req.url;
		// console.log("goto", redirect_location)

		res.writeHead(307, {
			"Location": redirect_location
		});

		res.end();
	}
});

const presets = {
	https: {
		cert: null,
		key: null
	},
	progress: () => {},
	before: ({ grant, wish, then }) => {
		then();
	},
	concluded: () => {},
	started: () => {}
}

module.exports = require("XO/start.xo")(presets, (macros) => {
	var holdings = new Holdings();
	var Macros = holdings.init()
	return new Macros(macros);
});
