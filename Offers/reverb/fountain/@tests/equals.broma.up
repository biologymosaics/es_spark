
// mocha '**/*.broma.up' --recursive --check-leaks --reporter spec -g "fountain equals"

const fountainsEqual = require("./../equals.up");

const video_short = require('Melody/@fixtures/C/index.xo');
const movie = require('Melody/@fixtures/B/index.xo');

describe("fountain equals", () => {
	describe("equality", () => {
		describe("16M Video", () => {
			it("works", function(done) {
				this.timeout(4000);

				const leaves = [
					video_short.original,
					video_short.mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						done();
					},
					no: () => {
						console.log("no??")
					}
				});
			});

			it("highWaterMark = 100", function(done) {
				this.timeout(4000);

				const leaves = [
					video_short.original,
					video_short.mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 100, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 100, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						done();
					},
					no: () => {
						console.log("no??")
					}
				});
			});

			it("non-equal highWaterMark's", function(done) {
				this.timeout(4000);

				const leaves = [
					video_short.original,
					video_short.mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 10000, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 1000, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						done();
					},
					no: () => {
						console.log("no??")
					}
				});
			});
		});

		describe("449M Video", () => {
			it("works", function(done) {
				this.timeout(4000);

				const leaves = [
					movie.mp4,
					movie.mp4Mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						done();
					},
					no: () => {
						console.log("no??")
					}
				});
			});
		});
	});


	describe("in-equality", () => {
		describe("16M Video", () => {
			it("start offset by 1", function(done) {
				this.timeout(4000);

				const leaves = [
					video_short.original,
					video_short.mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
					start: 1
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						console.log("yes??")
					},
					no: () => {
						done();
					}
				});
			});

			it("end at 65536 * 20", function(done) {
				this.timeout(4000);

				const leaves = [
					video_short.original,
					video_short.mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
					// end: 65536

					// actual end is this:
					// end: 16989956


					end: 65536 * 20,
					// end: 1310720,

					// end: 16989957 * 2
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						console.log("yes??")
					},
					no: () => {
						done();
					}
				});
			});
		});

		describe("449M Video", () => {
			it("start offset by 1", function(done) {
				this.timeout(4000);

				const leaves = [
					movie.mp4,
					movie.mp4Mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024

					start: 1
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024

					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						console.log("yes??")
					},
					no: () => {
						done();
					}
				});
			});

			it("end at 65536 * 20", function(done) {
				this.timeout(4000);

				const leaves = [
					movie.mp4,
					movie.mp4Mimic
				];

				var readableA = require('fs').createReadStream(leaves[0], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1 // 64 * 1024
					// end: 65536

					// actual end is this:
					// end: 16989956


					end: 65536 * 20,
					// end: 1310720,

					// end: 16989957 * 2
				});
				var readableB = require('fs').createReadStream(leaves[1], {
					highWaterMark: 65536, // 64 * 1024
					// highWaterMark: 100, // 64 * 1024
					// highWaterMark: 1
				});

				fountainsEqual({
					fountains: [
						readableA,
						readableB
					],
					yes: () => {
						console.log("yes??")
					},
					no: () => {
						done();
					}
				});
			});
		});
	});
});
